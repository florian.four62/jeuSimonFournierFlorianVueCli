import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/home.vue'
import About from '../views/about.vue'
import ListeJeu from '../views/listejeu.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    component: About
  },
  {
    path: '/simon',
    name: 'Simon',
    component: () => import('../views/simon.vue')
  },
  {
    path: '/listejeu',
    name: 'ListeJeu',
    component: ListeJeu
  }
]

const router = new VueRouter({
  routes
})

export default router
